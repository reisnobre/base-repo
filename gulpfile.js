var gulp        = require('gulp'),
    sass        = require('gulp-sass'),
    browserSync = require('browser-sync'),
    prefix      = require('gulp-autoprefixer'),
    cp          = require('child_process'),
    plumber     = require('gulp-plumber'),
    cleanCSS    = require('gulp-clean-css'),
    gulpif      = require('gulp-if'),
    imagemin    = require('gulp-imagemin'),
    uglify      = require('gulp-uglify'),
    jekyll      = process.platform === 'win32' ? 'jekyll.bat' : 'jekyll',
    messages    = {jekyllBuild: '<span style="color: grey">Running:</span> $ jekyll build'};


var env = process.env.NODE_ENV;
/**
 * Build the Jekyll Site
 */
gulp.task('jekyll-build', function (done) {
    browserSync.notify(messages.jekyllBuild);
    return cp.spawn( jekyll , ['build'], {stdio: 'inherit'})
        .on('close', done);
});

/**
 * Rebuild Jekyll & do page reload
 */
gulp.task('jekyll-rebuild', ['jekyll-build'], function () {
  browserSync.reload();
});

/**
 * Wait for jekyll-build, then launch the Server
 */
gulp.task('browser-sync', ['sass', 'jekyll-build','uglify'], function() {
  browserSync({
    server: {
      baseDir: '_site'
    },
    port: 8080
  });
});

/**
 * Compile files from _scss into both _site/css (for live injecting) and site (for future jekyll builds)
 */
gulp.task('sass', function () {
  var config = {};
  
  if (env === 'production'){
    config.outputStyle = ':compressed'
  }
  return gulp.src('assets/css/scss/main.scss')
    .pipe(sass(config).on('error', sass.logError))
    .pipe(plumber())
    .pipe(prefix(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
    .pipe(gulp.dest('assets/css'))
    .pipe(sass(config).on('error', sass.logError))
    .pipe(gulp.dest('_site/assets/css'))
    .pipe(browserSync.reload({stream:true}))
});

gulp.task('uglify', function() {
    gulp.src('assets/js/*')
      .pipe(uglify())
      .pipe(gulp.dest('_site/assets/js'))
});

gulp.task('image', function() {
  gulp.src('assets/img/**/*')
    .pipe(imagemin())
    .pipe(gulp.dest('_site/assets/img'))
});

/**
 * Watch scss files for changes & recompile
 * Watch html/md files, run jekyll & reload BrowserSync
 */

gulp.task('watch', function () {
    gulp.watch('assets/css/scss/**/*.scss', ['sass']);
    gulp.watch(['*.html', '_layouts/*.html', '_posts/**/*.*','_includes/*.html','_portifolio/*.html','assets/js/*.js','assets/img/**/*.*' ], ['jekyll-rebuild']);
});

/**
 * Default task, running just `gulp` will compile the sass,
 * compile the jekyll site, launch BrowserSync & watch files.
 */
gulp.task('default', ['image','browser-sync', 'watch']);
