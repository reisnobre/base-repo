var ap1 = new APlayer({
    element: document.getElementById('player1'),
        narrow: false,
        autoplay: false,
        showlrc: false,
        mutex: true,
        theme: 'white',
        music: [
        {
            title: 'Samba da Bailarina',
            author: '',
            url: 'http://k003.kiwi6.com/hotlink/bq6qjosouq/samba_da_bailarina.mp3',
            pic: '/assets/img/albums/bailarina.jpg'
        },
        {
            title: 'Samba Forte',
            author: '',
            url: 'http://k003.kiwi6.com/hotlink/odto5zrzk7/samba_forte.mp3',
            pic: '/assets/img/albums/autoraisFolders.jpg'
        },
        {
            title: 'A música',
            author: '',
            url: 'http://k003.kiwi6.com/hotlink/y9n1zmsioh/a_m_sica.mp3',
            pic: '/assets/img/albums/autoraisFolders.jpg'
        },
        {
            title: 'Pra não virar vício',
            author: '',
            url: 'http://k003.kiwi6.com/hotlink/b5huse29tx/pra_nao_virar_v_cio.mp3',
            pic: '/assets/img/albums/autoraisFolders.jpg'
        },
        {
            title: 'Olhos frios',
            author: '',
            url: 'http://k003.kiwi6.com/hotlink/1wjnm676y7/Olhos_frios.mp3',
            pic: '/assets/img/albums/autoraisFolders.jpg'
        }
    ]
});

var ap2 = new APlayer({
    element: document.getElementById('player2'),
    narrow: false,
    autoplay: false,
    showlrc: false,
    mutex: true,
    theme: 'white',
    music: [
        {
            title: 'Eu amo Você',
            author: '',
            url: 'http://k003.kiwi6.com/hotlink/cep6nsxgqf/eu_amo_vc.mp3',
            pic: '/assets/img/albums/parceriasFolders.jpg'
        },
        {
            title: 'Dindi',
            author: '',
            url: 'http://k003.kiwi6.com/hotlink/outwyst2rb/dindi.mp3',
            pic: '/assets/img/albums/parceriasFolders.jpg'
        },
        {
            title: 'Angel',
            author: '',
            url: 'http://k003.kiwi6.com/hotlink/chy2axa8k4/angel.mp3',
            pic: '/assets/img/albums/parceriasFolders.jpg'
        }    
    ]
});

var ap3 = new APlayer({
    element: document.getElementById('player3'),
        narrow: false,
        autoplay: false,
        showlrc: false,
        mutex: true,
        theme: 'white',
        music: [
            {
                title: 'Bar Romance',
                author: '',
                url: 'http://k003.kiwi6.com/hotlink/kv4emdigos/bad_romance.mp3',
                pic: '/assets/img/albums/coverFolders.jpg'
            },
            {
                title: 'Eclipse Oculto',
                author: '',
                url: 'http://k003.kiwi6.com/hotlink/u7ujc29lhm/eclipse_oulto.mp3',
                pic: '/assets/img/albums/coverFolders.jpg'
            },
            {
                title: 'Hit the Road Jack',
                author: '',
                url: 'http://k003.kiwi6.com/hotlink/gjije1dydq/hit_the_road_jack.mp3',
                pic: '/assets/img/albums/coverFolders.jpg'
            },
            {
                title: 'Back to  Black',
                author: '',
                url: 'http://k003.kiwi6.com/hotlink/lbpuz50m3m/back_to_black.mp3',
                pic: '/assets/img/albums/coverFolders.jpg'
            },
            {
                title: 'Diamonds',
                author: '',
                url: 'http://k003.kiwi6.com/hotlink/5ttqen85or/diamonds.mp3',
                pic: '/assets/img/albums/coverFolders.jpg'
            },
            {
                title: 'Metade',
                author: '',
                url: 'http://k003.kiwi6.com/hotlink/bxj9q68ljb/6._Metade.mp3',
                pic: '/assets/img/albums/coverFolderSCFM.jpg'
            },
            {
                title: 'Segue o Seco',
                author: '',
                url: 'http://k003.kiwi6.com/hotlink/pf110p4ugk/segue_o_seco.mp3',
                pic: '/assets/img/albums/coverFolderSCFM.jpg'
            },
            {
                title: 'Valerie',
                author: '',
                url: 'http://k003.kiwi6.com/hotlink/quj2okdj6q/valerie.mp3',
                pic: '/assets/img/albums/coverFolderSCFM.jpg'
            },
            {
                title: 'O que Queres',
                author: '',
                url: 'http://k003.kiwi6.com/hotlink/nuros4a22a/o_quereres.mp3',
                pic: '/assets/img/albums/coverFolderSCFM.jpg'
            },
            {
                title: 'Cordeiro de Nanan',
                author: '',
                url: 'http://k003.kiwi6.com/hotlink/cu6xrb8mhk/10._Cordeiro_de_nan_.mp3',
                pic: '/assets/img/albums/coverFolderSCFM.jpg'
            }
        ]
});

ap1.init();
ap2.init();
ap3.init();
