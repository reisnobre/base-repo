$(document).ready(function(){
   $('.swiper-musicas').mCustomScrollbar({
       axis: "y",
      //  scrollInertia: 3
   });
   $('.topPart').mCustomScrollbar({
       axis: "x",
       scrollInertia: 1,
       theme: "light"
   });
   $('.textCrop').mCustomScrollbar({
       axis: "y",
       scrollInertia: 0,
       theme: "dark-2"
   });
   
});