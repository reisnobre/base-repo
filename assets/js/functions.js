(function ($) {
    var $mobileNavToggleBtn = $('.mobileToggle');
    var $mobileNavRemovers = $('.mobileHeader, .mainContent');
    var $modalBtn = $('.postLinks');
    var $closeBtn = $('.fa-times , .modalHolder');
    var $yTChangeBtn = $('.yTBtn');
    var $fBChangeBtn = $('.fbBtn');
    var $agendaBtn = $('.ctrlItem');
    var $agendaScrollTrigger = $('.rightSide');
    var $agendaBtn = $('.days');
    var i = 1;

    function onBtnClick (e) {
        var $this = $(this), $selectors = $('.mobileToggle, .mobileHeader, .mainContent, .feedContainer');

        $this.toggleClass('nowOpen');
        $selectors.toggleClass('nowOpen');
        $mobileNavToggleBtn.toggleClass('nowOpen');
    }

    function onClickRemove(e) {
        var $this = $(this);


        $this.removeClass('nowOpen');
        $mobileNavRemovers.removeClass('nowOpen');
        $('.mobileToggle').removeClass('nowOpen');
    }

    function modalController(e) {
        var $this = $(this),
        $modalBtn = $('.postLinks'),
        $modalSrc = $this.attr('href');

        $('.modal iframe').attr('src', $modalSrc);
        $('.modalHolder').addClass('nowOpen');
        $('.wrapper').addClass('closed');
    }

    function closeModal(e) {
        $('.modalHolder').removeClass('nowOpen');
        $('.wrapper').removeClass('closed');
    }

    function yTHandler(e) {
        var $this = $(this), $yTVideo = $('.videoContainer'), videoSrc = $this.attr('yTLink');

        $yTVideo.remove();
        $('<iframe class="videoContainer" frameborder="0" allowfullscreen></iframe>')
        .attr("src", "https://www.youtube.com/embed/"+videoSrc)
        .appendTo('.bottomPart');

    }

    function fBHandler (e) {
        var $this = $(this), $yTVideo = $('.videoContainer'), videoSrc = $this.attr('fBLink');

        $yTVideo.remove();
        $('<iframe class="videoContainer" frameborder="0" allowfullscreen></iframe>')
        .attr("src", videoSrc)
        .appendTo('.bottomPart');
    }


    if($('#instafeed')){
        var feed = new Instafeed({
            get: 'tagged',
            tagName: 'teste',
            sortBy: 'most-recent',
            clientId: '647cd4958ec1461ca5d13373a68d806a',
            userId: '3565220559',
            accessToken: '3565220559.647cd49.7af161211e6c46de8cd4e084b22c0def',
            template: '<a href="{{link}}" target="_blank"><img src="{{image}}" /></a>',
            resolution: "low_resolution",
            limit: 3
        });
        feed.run();
    }

    function gridGal () {
        var $grid = $('.grid').masonry({
            transitionDuration: '0.2s',
            itemSelector: '.grid-item',
            columnWidth: '.grid-item',
            percentPosition: true,
            fitWidth: true
        });
        $grid.imagesLoaded().progress( function() {
            $grid.masonry('layout');
        });
    }
    
    function showChange (e) {
        $('.selected').removeClass('selected');
        $(this).addClass('selected');
        $('.showInfo').attr('src', $(this).attr('href'));
    }
    
    function musAdjust (){
        dWidth = $(document).width();
        if(dWidth < 500){
            dHeight = $(document).height();
            $('.swiper-container').css("height",dHeight-110);
            $('.muscBg').css("height",dHeight);
            if(dHeight <= 530){
                $('.upperPart').css("height","220px")
            }
        }
    }
    $(document).ready(function () {
        $mobileNavToggleBtn.on('click', onBtnClick);
        $mobileNavRemovers.on('click', onClickRemove);
        $modalBtn.on('click', modalController);
        $closeBtn.on('click', closeModal);
        $yTChangeBtn.on('click', yTHandler);
        $fBChangeBtn.on('click', fBHandler);
        $agendaBtn.on('click', showChange)
        
        $agendaBtn.first().addClass('selected')
        $('.showInfo').attr('src', $agendaBtn.first().attr('href'));
        musAdjust();
    });
}) (jQuery);
